import React from "react";
import { Table, Button } from "react-bootstrap";

export default function MyTable({ students, handleSelect, handleDelete }) {
  return (
    <div style={{ marginLeft: "50px" }}>
      <h3>Table Accounts</h3>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>PublicshedYear</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {students.map((item, i) => (
            <tr
              onClick={() => handleSelect(i)}
              style={{ backgroundColor: item.isClick ? "blue" : "unset" }}
            >
              <td>{i + 1}</td>
              <td>{item.username}</td>
              <td>{item.email}</td>
              <td>{item.gender}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Button variant="danger" onClick={handleDelete}>
        Delete
      </Button>
    </div>
  );
}
