import React from "react";
import { Component } from "react";
import { Form, Col, Button } from "react-bootstrap";

export default class Account extends Component {
  render() {
    return (
      <div>
        <h3>Create Account</h3>
        <Form.Group>
          <Form.Group>
            <Form.Label column sm={-1}>
              Username
            </Form.Label>
            <Col sm={20}>
              <Form.Control
                onChange={this.props.handleChange}
                type="email"
                placeholder="Username"
                name="username"
                id="username"
              />
            </Col>
          </Form.Group>
          <fieldset>
            <Form.Group>
              <Form.Label as="legend" column sm={-1}>
                Gender
              </Form.Label>
              <Col sm={20} style={{ display: "flex" }}>
                <Form.Check
                  onChange={this.props.handleChange}
                  type="radio"
                  label="male"
                  name="gender"
                  id="gender"
                  value="male"
                />
                <Form.Check
                  style={{ marginLeft: "10px" }}
                  onChange={this.props.handleChange}
                  type="radio"
                  label="female"
                  name="gender"
                  id="gender"
                  value="female"
                />
              </Col>
            </Form.Group>
          </fieldset>
          <Form.Group>
            <Form.Label column sm={-1}>
              Email
            </Form.Label>
            <Col sm={20}>
              <Form.Control
                onChange={this.props.handleChange}
                type="email"
                placeholder="Email"
                name="email"
                id="email"
              />
            </Col>
          </Form.Group>

          <Form.Group controlId="formHorizontalPassword">
            <Form.Label column sm={-1}>
              Password
            </Form.Label>
            <Col sm={20}>
              <Form.Control
                onChange={this.props.handleChange}
                type="password"
                placeholder="Password"
                name="password"
                id="password"
              />
            </Col>
          </Form.Group>

          <Form.Group>
            <Col column sm={-1}>
              <Button variant="primary" onClick={this.props.handleAdd}>
                Save
              </Button>{" "}
            </Col>
          </Form.Group>
        </Form.Group>
      </div>
    );
  }
}
