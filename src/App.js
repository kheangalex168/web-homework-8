import "bootstrap/dist/css/bootstrap.min.css";
import MyTable from "./components/Table";
import Account from "./components/Account";
import React from "react";
import { Container, Row } from "react-bootstrap";
import { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      students: [
        {
          id: 1,
          username: "Nana",
          email: "apple@icloud.com",
          gender: "male"
        },
        {
          id: 2,
          username: "Kaka",
          email: "apple@icloud.com",
          gender: "male"
        },
        {
          id: 3,
          username: "Momo",
          email: "apple@icloud.com",
          gender: "female"
        }
      ]
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleAdd = (e) => {
    e.preventDefault();
    if (this.state.username === "") {
      return;
    }
    const newItem = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
      gender: this.state.gender
    };
    this.setState((state) => ({
      students: state.students.concat(newItem),
      username: "",
      email: "",
      password: "",
      gender: "",
    }));
    document.getElementById('username').value = ""
    var ele = document.getElementsByName("gender");
    for(var i=0;i<ele.length;i++)
       ele[i].checked = false;
    document.getElementById('email').value = ""
    document.getElementById('password').value = ""
  };

  handleSelect = (i) => {
    let tmp = [...this.state.students];
    tmp[i].isClick = !tmp[i].isClick;
    this.setState({
      students: tmp
    });
  };

  handleDelete = () => {
    let tmp = [...this.state.students];
    let temp = tmp.filter((item, i) => {
      return !tmp[i].isClick;
    });
    this.setState({
      students: temp
    });
  };

  render() {
    return (
      <Container>
        <Row>
          <p>KSHRD Student</p>
        </Row>
        <Row>
          <Account
            handleAdd={this.handleAdd}
            handleChange={this.handleChange}
            tmp={this.state.tmp}
          />
          <MyTable
            students={this.state.students}
            handleSelect={this.handleSelect}
            handleDelete={this.handleDelete}
          />
        </Row>
      </Container>
    );
  }
}

export default App;

